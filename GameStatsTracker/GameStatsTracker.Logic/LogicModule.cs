﻿using Autofac;
using GameStatsTracker.Logic.Session;

namespace GameStatsTracker.Logic
{
	public class LogicModule : Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			base.Load(builder);

			builder.RegisterType<GameStatsProvider>();
            builder.RegisterType<SessionManager>().As<ISessionManager>().SingleInstance();
		}
	}
}