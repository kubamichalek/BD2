﻿using GameStatsTracker.DataAccess;

namespace GameStatsTracker.Logic.Session
{
    public class SessionManager : ISessionManager
    {
        public User LoggedUser { get; set; }
        public bool LoggedOut { get; set; }
        public Game MyGame { get; set; }
        public GameAchievement UserGameAchievement { get; set; }
    }
}
