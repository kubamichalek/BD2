﻿using GameStatsTracker.DataAccess;

namespace GameStatsTracker.Logic.Session
{
    public interface ISessionManager
    {
        User LoggedUser { get; set; }
        bool LoggedOut { get; set; }
        Game MyGame { get; set; }
        GameAchievement UserGameAchievement { get; set; }
    }
}
