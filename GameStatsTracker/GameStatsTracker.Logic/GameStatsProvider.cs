using GameStatsTracker.DataAccess;
using System.Collections.Generic;
using System.Linq;
using System;

namespace GameStatsTracker.Logic
{
	public class GameStatsProvider
	{
		private readonly EntityAccessor _accessor;

		public GameStatsProvider(EntityAccessor accessor)
		{
			_accessor = accessor;
		}

		public GetUser GetUsers()
		{
			return new GetUser(_accessor);
		}

		public GetGame GetGames()
		{
			return new GetGame(_accessor);
		}

		public GetAchievement GetAchievements()
		{
			return new GetAchievement(_accessor);
		}

		public GetFacility GetFacilities()
		{
			return new GetFacility(_accessor);
		}

		public GetGenre GetGenres()
		{
			return new GetGenre(_accessor);
		}

		public RegisterUser RegisterUser()
		{
			return new RegisterUser(_accessor);
		}

        public GetUser GetUser()
        {
            return new GetUser(_accessor);
        }
    }

	public class RegisterUser
	{
		private readonly EntityAccessor _accessor;

		public RegisterUser(EntityAccessor accessor)
		{
			_accessor = accessor;
		}

		public void SignUp(User user)
		{
			User exists = _accessor.Users.FirstOrDefault(u => u.Email == user.Email);
			if (exists != null)
			{
				throw new UserAlreadyExistsException();
			}

			_accessor.Users.Add(user);
			_accessor.Save();
		}
	}

	public class GetGenre
	{
		private readonly EntityAccessor _accessor;

		public GetGenre(EntityAccessor accessor)
		{
			_accessor = accessor;
		}

		public IEnumerable<Genre> All()
		{
			return _accessor.Genres;
		}
	}

	public class GetFacility
	{
		private readonly EntityAccessor _accessor;

		public GetFacility(EntityAccessor accessor)
		{
			_accessor = accessor;
		}

		public IEnumerable<Facility> All()
		{
			return _accessor.Facilities;
		}
	}

	public class GetAchievement
	{
		private readonly EntityAccessor _accessor;

		public GetAchievement(EntityAccessor accessor)
		{
			_accessor = accessor;
		}

		public IEnumerable<GameAchievement> All()
		{
			return _accessor.Achievements;
		}

        public GameAchievement WhereID(int ID)
        {
            return All().FirstOrDefault(u => u.Game.Id == ID);
        }
    }

	public class GetGame
	{
		private readonly EntityAccessor _accessor;

		public GetGame(EntityAccessor accessor)
		{
			_accessor = accessor;
		}

		public IEnumerable<Game> All()
		{
			return _accessor.Games;
		}

        public Game WithName(string name)
        {
            return All().FirstOrDefault(u => u.Name == name);
        }
    }

	public class GetUser
	{
		private readonly EntityAccessor _accessor;

		public GetUser(EntityAccessor accessor)
		{
			_accessor = accessor;
		}

		public IEnumerable<User> All()
		{
			return _accessor.Users;
		}

		public bool HavingEmailOrNicknameAndPassword(string emailOrNickname, string password)
		{
			return All().Any(user => (user.NickName == emailOrNickname || user.Email == emailOrNickname) && user.Password == password);
		}

        public User WithEmailOrNickName(string emailOrNickname)
        {
            return All().FirstOrDefault(u => u.Email == emailOrNickname || u.NickName == emailOrNickname);
        }
    }
}