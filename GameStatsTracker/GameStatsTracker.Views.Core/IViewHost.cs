﻿using System.ComponentModel;
using System.Windows.Controls;

namespace GameStatsTracker.Views.Core
{
	public interface IViewHost : INotifyPropertyChanged
	{
		ContentControl CurrentView { get; }

		void GoToView(string viewName);
	}
}