﻿namespace GameStatsTracker.Views.Core
{
	public static class ViewNames
	{
		public const string MAIN_WINDOW = "MainWindow";
		public const string MAIN_MENU_PAGE = "MainMenu";
		public const string ABOUT_PAGE = "AboutPage";
		public const string SIGN_IN_PAGE = "SignInPage";
		public const string SIGN_UP_PAGE = "SignUpPage";
		public const string STATS_PAGE = "StatsPage";
        public const string PLAYER_STATS_PAGE = "PlayerStatsPage";
        public const string GAME_STATS_PAGE = "GameStatsPage";
        public const string LOGGED_USER_MENU_PAGE = "LoggedUserMenuPage";
        public const string ACHIEVEMENT_PAGE = "AchievementPage";
        public const string SURVEYS_LIST_PAGE = "SurveysListPage";
        public const string SURVEY_PAGE = "SurveyPage";
        public const string SURVEY_RESULT_PAGE = "SurveyResultPage";
    }
}