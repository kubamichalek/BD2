using Autofac.Features.Indexed;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Controls;

namespace GameStatsTracker.Views.Core
{
	public class ViewHost : IViewHost
	{
		private readonly IIndex<string, ContentControl> _views;
		private ContentControl _currentView;

		public ViewHost(IIndex<string, ContentControl> views)
		{
			_views = views;
		}

		public ContentControl CurrentView
		{
			get => _currentView;
			set
			{
				_currentView = value;
				OnPropertyChanged();
			}
		}

		public void GoToView(string viewName)
		{
			CurrentView = _views[viewName];
		}

		public event PropertyChangedEventHandler PropertyChanged;

		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}