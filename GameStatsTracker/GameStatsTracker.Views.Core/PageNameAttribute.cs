﻿using System;

namespace GameStatsTracker.Views.Core
{
	public class PageNameAttribute : Attribute
	{
		public string Name { get; }

		public PageNameAttribute(string name)
		{
			Name = name;
		}
	}
}