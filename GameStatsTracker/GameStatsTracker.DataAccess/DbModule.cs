﻿using Autofac;

namespace GameStatsTracker.DataAccess
{
	public class DbModule : Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			builder.RegisterType<EntityAccessor>().AsSelf().SingleInstance();
		}
	}
}