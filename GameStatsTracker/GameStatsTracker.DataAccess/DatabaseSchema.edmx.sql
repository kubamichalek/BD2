
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 06/18/2017 11:12:48
-- Generated from EDMX file: C:\Users\Adam\Source\Repos\BD2\GameStatsTracker\GameStatsTracker.DataAccess\DatabaseSchema.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [GameStatsTracker];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_GamePlayer_Game]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GamePlayer] DROP CONSTRAINT [FK_GamePlayer_Game];
GO
IF OBJECT_ID(N'[dbo].[FK_GamePlayer_User]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GamePlayer] DROP CONSTRAINT [FK_GamePlayer_User];
GO
IF OBJECT_ID(N'[dbo].[FK_PlayerSurveyQuestionAnswer]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SurveyQuestionAnswers] DROP CONSTRAINT [FK_PlayerSurveyQuestionAnswer];
GO
IF OBJECT_ID(N'[dbo].[FK_PlayerFacility_User]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PlayerFacility] DROP CONSTRAINT [FK_PlayerFacility_User];
GO
IF OBJECT_ID(N'[dbo].[FK_PlayerFacility_Facility]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PlayerFacility] DROP CONSTRAINT [FK_PlayerFacility_Facility];
GO
IF OBJECT_ID(N'[dbo].[FK_GenreGame_Genre]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenreGame] DROP CONSTRAINT [FK_GenreGame_Genre];
GO
IF OBJECT_ID(N'[dbo].[FK_GenreGame_Game]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenreGame] DROP CONSTRAINT [FK_GenreGame_Game];
GO
IF OBJECT_ID(N'[dbo].[FK_GameGameAchievment]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GameAchievements] DROP CONSTRAINT [FK_GameGameAchievment];
GO
IF OBJECT_ID(N'[dbo].[FK_GameLevel]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Levels] DROP CONSTRAINT [FK_GameLevel];
GO
IF OBJECT_ID(N'[dbo].[FK_SurveySurveyQuestion]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SurveyQuestions] DROP CONSTRAINT [FK_SurveySurveyQuestion];
GO
IF OBJECT_ID(N'[dbo].[FK_GameSurvey]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Surveys] DROP CONSTRAINT [FK_GameSurvey];
GO
IF OBJECT_ID(N'[dbo].[FK_SurveyQuestionSurveyQuestionAnswer]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SurveyQuestionAnswers] DROP CONSTRAINT [FK_SurveyQuestionSurveyQuestionAnswer];
GO
IF OBJECT_ID(N'[dbo].[FK_GameFacility_Game]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GameFacility] DROP CONSTRAINT [FK_GameFacility_Game];
GO
IF OBJECT_ID(N'[dbo].[FK_GameFacility_Facility]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GameFacility] DROP CONSTRAINT [FK_GameFacility_Facility];
GO
IF OBJECT_ID(N'[dbo].[FK_NumericAchievement_inherits_GameAchievement]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GameAchievements_NumericAchievement] DROP CONSTRAINT [FK_NumericAchievement_inherits_GameAchievement];
GO
IF OBJECT_ID(N'[dbo].[FK_TextAchievement_inherits_GameAchievement]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GameAchievements_TextAchievement] DROP CONSTRAINT [FK_TextAchievement_inherits_GameAchievement];
GO
IF OBJECT_ID(N'[dbo].[FK_LogicAchievement_inherits_GameAchievement]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GameAchievements_LogicAchievement] DROP CONSTRAINT [FK_LogicAchievement_inherits_GameAchievement];
GO
IF OBJECT_ID(N'[dbo].[FK_TimeAchievement_inherits_GameAchievement]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GameAchievements_TimeAchievement] DROP CONSTRAINT [FK_TimeAchievement_inherits_GameAchievement];
GO
IF OBJECT_ID(N'[dbo].[FK_YesNoSurveyQuestionAnswer_inherits_SurveyQuestionAnswer]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SurveyQuestionAnswers_YesNoSurveyQuestionAnswer] DROP CONSTRAINT [FK_YesNoSurveyQuestionAnswer_inherits_SurveyQuestionAnswer];
GO
IF OBJECT_ID(N'[dbo].[FK_TextSurveyQuestionAnswer_inherits_SurveyQuestionAnswer]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SurveyQuestionAnswers_TextSurveyQuestionAnswer] DROP CONSTRAINT [FK_TextSurveyQuestionAnswer_inherits_SurveyQuestionAnswer];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Users]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Users];
GO
IF OBJECT_ID(N'[dbo].[Games]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Games];
GO
IF OBJECT_ID(N'[dbo].[Genres]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Genres];
GO
IF OBJECT_ID(N'[dbo].[GameAchievements]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GameAchievements];
GO
IF OBJECT_ID(N'[dbo].[Levels]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Levels];
GO
IF OBJECT_ID(N'[dbo].[Surveys]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Surveys];
GO
IF OBJECT_ID(N'[dbo].[SurveyQuestions]', 'U') IS NOT NULL
    DROP TABLE [dbo].[SurveyQuestions];
GO
IF OBJECT_ID(N'[dbo].[SurveyQuestionAnswers]', 'U') IS NOT NULL
    DROP TABLE [dbo].[SurveyQuestionAnswers];
GO
IF OBJECT_ID(N'[dbo].[Facilities]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Facilities];
GO
IF OBJECT_ID(N'[dbo].[GameAchievements_NumericAchievement]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GameAchievements_NumericAchievement];
GO
IF OBJECT_ID(N'[dbo].[GameAchievements_TextAchievement]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GameAchievements_TextAchievement];
GO
IF OBJECT_ID(N'[dbo].[GameAchievements_LogicAchievement]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GameAchievements_LogicAchievement];
GO
IF OBJECT_ID(N'[dbo].[GameAchievements_TimeAchievement]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GameAchievements_TimeAchievement];
GO
IF OBJECT_ID(N'[dbo].[SurveyQuestionAnswers_YesNoSurveyQuestionAnswer]', 'U') IS NOT NULL
    DROP TABLE [dbo].[SurveyQuestionAnswers_YesNoSurveyQuestionAnswer];
GO
IF OBJECT_ID(N'[dbo].[SurveyQuestionAnswers_TextSurveyQuestionAnswer]', 'U') IS NOT NULL
    DROP TABLE [dbo].[SurveyQuestionAnswers_TextSurveyQuestionAnswer];
GO
IF OBJECT_ID(N'[dbo].[GamePlayer]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GamePlayer];
GO
IF OBJECT_ID(N'[dbo].[PlayerFacility]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PlayerFacility];
GO
IF OBJECT_ID(N'[dbo].[GenreGame]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenreGame];
GO
IF OBJECT_ID(N'[dbo].[GameFacility]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GameFacility];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Users'
CREATE TABLE [dbo].[Users] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [NickName] nvarchar(max)  NOT NULL,
    [DateOfBirth] datetime  NOT NULL,
    [Role] int  NOT NULL,
    [Password] nvarchar(max)  NOT NULL,
    [Email] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Games'
CREATE TABLE [dbo].[Games] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [ReleaseDate] datetime  NOT NULL,
    [Description] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Genres'
CREATE TABLE [dbo].[Genres] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'GameAchievements'
CREATE TABLE [dbo].[GameAchievements] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Priority] int  NOT NULL,
    [Game_Id] int  NOT NULL
);
GO

-- Creating table 'Levels'
CREATE TABLE [dbo].[Levels] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Game_Id] int  NOT NULL
);
GO

-- Creating table 'Surveys'
CREATE TABLE [dbo].[Surveys] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Game_Id] int  NOT NULL
);
GO

-- Creating table 'SurveyQuestions'
CREATE TABLE [dbo].[SurveyQuestions] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Question] nvarchar(max)  NOT NULL,
    [Survey_Id] int  NOT NULL
);
GO

-- Creating table 'SurveyQuestionAnswers'
CREATE TABLE [dbo].[SurveyQuestionAnswers] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [User_Id] int  NOT NULL,
    [Question_Id] int  NOT NULL
);
GO

-- Creating table 'Facilities'
CREATE TABLE [dbo].[Facilities] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'GameAchievements_NumericAchievement'
CREATE TABLE [dbo].[GameAchievements_NumericAchievement] (
    [Value] int  NOT NULL,
    [Id] int  NOT NULL
);
GO

-- Creating table 'GameAchievements_TextAchievement'
CREATE TABLE [dbo].[GameAchievements_TextAchievement] (
    [Value] nvarchar(max)  NOT NULL,
    [Id] int  NOT NULL
);
GO

-- Creating table 'GameAchievements_LogicAchievement'
CREATE TABLE [dbo].[GameAchievements_LogicAchievement] (
    [Value] bit  NOT NULL,
    [Id] int  NOT NULL
);
GO

-- Creating table 'GameAchievements_TimeAchievement'
CREATE TABLE [dbo].[GameAchievements_TimeAchievement] (
    [Value] time  NOT NULL,
    [Id] int  NOT NULL
);
GO

-- Creating table 'SurveyQuestionAnswers_YesNoSurveyQuestionAnswer'
CREATE TABLE [dbo].[SurveyQuestionAnswers_YesNoSurveyQuestionAnswer] (
    [Answer] bit  NOT NULL,
    [Id] int  NOT NULL
);
GO

-- Creating table 'SurveyQuestionAnswers_TextSurveyQuestionAnswer'
CREATE TABLE [dbo].[SurveyQuestionAnswers_TextSurveyQuestionAnswer] (
    [Answer] nvarchar(max)  NOT NULL,
    [Id] int  NOT NULL
);
GO

-- Creating table 'GamePlayer'
CREATE TABLE [dbo].[GamePlayer] (
    [Games_Id] int  NOT NULL,
    [Users_Id] int  NOT NULL
);
GO

-- Creating table 'PlayerFacility'
CREATE TABLE [dbo].[PlayerFacility] (
    [Users_Id] int  NOT NULL,
    [GameFacilities_Id] int  NOT NULL
);
GO

-- Creating table 'GenreGame'
CREATE TABLE [dbo].[GenreGame] (
    [Genres_Id] int  NOT NULL,
    [Games_Id] int  NOT NULL
);
GO

-- Creating table 'GameFacility'
CREATE TABLE [dbo].[GameFacility] (
    [Games_Id] int  NOT NULL,
    [Facilities_Id] int  NOT NULL
);
GO

-- Creating table 'GameAchievementUser'
CREATE TABLE [dbo].[GameAchievementUser] (
    [GameAchievements_Id] int  NOT NULL,
    [Users_Id] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'Users'
ALTER TABLE [dbo].[Users]
ADD CONSTRAINT [PK_Users]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Games'
ALTER TABLE [dbo].[Games]
ADD CONSTRAINT [PK_Games]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Genres'
ALTER TABLE [dbo].[Genres]
ADD CONSTRAINT [PK_Genres]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'GameAchievements'
ALTER TABLE [dbo].[GameAchievements]
ADD CONSTRAINT [PK_GameAchievements]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Levels'
ALTER TABLE [dbo].[Levels]
ADD CONSTRAINT [PK_Levels]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Surveys'
ALTER TABLE [dbo].[Surveys]
ADD CONSTRAINT [PK_Surveys]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'SurveyQuestions'
ALTER TABLE [dbo].[SurveyQuestions]
ADD CONSTRAINT [PK_SurveyQuestions]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'SurveyQuestionAnswers'
ALTER TABLE [dbo].[SurveyQuestionAnswers]
ADD CONSTRAINT [PK_SurveyQuestionAnswers]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Facilities'
ALTER TABLE [dbo].[Facilities]
ADD CONSTRAINT [PK_Facilities]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'GameAchievements_NumericAchievement'
ALTER TABLE [dbo].[GameAchievements_NumericAchievement]
ADD CONSTRAINT [PK_GameAchievements_NumericAchievement]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'GameAchievements_TextAchievement'
ALTER TABLE [dbo].[GameAchievements_TextAchievement]
ADD CONSTRAINT [PK_GameAchievements_TextAchievement]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'GameAchievements_LogicAchievement'
ALTER TABLE [dbo].[GameAchievements_LogicAchievement]
ADD CONSTRAINT [PK_GameAchievements_LogicAchievement]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'GameAchievements_TimeAchievement'
ALTER TABLE [dbo].[GameAchievements_TimeAchievement]
ADD CONSTRAINT [PK_GameAchievements_TimeAchievement]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'SurveyQuestionAnswers_YesNoSurveyQuestionAnswer'
ALTER TABLE [dbo].[SurveyQuestionAnswers_YesNoSurveyQuestionAnswer]
ADD CONSTRAINT [PK_SurveyQuestionAnswers_YesNoSurveyQuestionAnswer]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'SurveyQuestionAnswers_TextSurveyQuestionAnswer'
ALTER TABLE [dbo].[SurveyQuestionAnswers_TextSurveyQuestionAnswer]
ADD CONSTRAINT [PK_SurveyQuestionAnswers_TextSurveyQuestionAnswer]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Games_Id], [Users_Id] in table 'GamePlayer'
ALTER TABLE [dbo].[GamePlayer]
ADD CONSTRAINT [PK_GamePlayer]
    PRIMARY KEY CLUSTERED ([Games_Id], [Users_Id] ASC);
GO

-- Creating primary key on [Users_Id], [GameFacilities_Id] in table 'PlayerFacility'
ALTER TABLE [dbo].[PlayerFacility]
ADD CONSTRAINT [PK_PlayerFacility]
    PRIMARY KEY CLUSTERED ([Users_Id], [GameFacilities_Id] ASC);
GO

-- Creating primary key on [Genres_Id], [Games_Id] in table 'GenreGame'
ALTER TABLE [dbo].[GenreGame]
ADD CONSTRAINT [PK_GenreGame]
    PRIMARY KEY CLUSTERED ([Genres_Id], [Games_Id] ASC);
GO

-- Creating primary key on [Games_Id], [Facilities_Id] in table 'GameFacility'
ALTER TABLE [dbo].[GameFacility]
ADD CONSTRAINT [PK_GameFacility]
    PRIMARY KEY CLUSTERED ([Games_Id], [Facilities_Id] ASC);
GO

-- Creating primary key on [GameAchievements_Id], [Users_Id] in table 'GameAchievementUser'
ALTER TABLE [dbo].[GameAchievementUser]
ADD CONSTRAINT [PK_GameAchievementUser]
    PRIMARY KEY CLUSTERED ([GameAchievements_Id], [Users_Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [Games_Id] in table 'GamePlayer'
ALTER TABLE [dbo].[GamePlayer]
ADD CONSTRAINT [FK_GamePlayer_Game]
    FOREIGN KEY ([Games_Id])
    REFERENCES [dbo].[Games]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Users_Id] in table 'GamePlayer'
ALTER TABLE [dbo].[GamePlayer]
ADD CONSTRAINT [FK_GamePlayer_User]
    FOREIGN KEY ([Users_Id])
    REFERENCES [dbo].[Users]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GamePlayer_User'
CREATE INDEX [IX_FK_GamePlayer_User]
ON [dbo].[GamePlayer]
    ([Users_Id]);
GO

-- Creating foreign key on [User_Id] in table 'SurveyQuestionAnswers'
ALTER TABLE [dbo].[SurveyQuestionAnswers]
ADD CONSTRAINT [FK_PlayerSurveyQuestionAnswer]
    FOREIGN KEY ([User_Id])
    REFERENCES [dbo].[Users]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PlayerSurveyQuestionAnswer'
CREATE INDEX [IX_FK_PlayerSurveyQuestionAnswer]
ON [dbo].[SurveyQuestionAnswers]
    ([User_Id]);
GO

-- Creating foreign key on [Users_Id] in table 'PlayerFacility'
ALTER TABLE [dbo].[PlayerFacility]
ADD CONSTRAINT [FK_PlayerFacility_User]
    FOREIGN KEY ([Users_Id])
    REFERENCES [dbo].[Users]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [GameFacilities_Id] in table 'PlayerFacility'
ALTER TABLE [dbo].[PlayerFacility]
ADD CONSTRAINT [FK_PlayerFacility_Facility]
    FOREIGN KEY ([GameFacilities_Id])
    REFERENCES [dbo].[Facilities]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PlayerFacility_Facility'
CREATE INDEX [IX_FK_PlayerFacility_Facility]
ON [dbo].[PlayerFacility]
    ([GameFacilities_Id]);
GO

-- Creating foreign key on [Genres_Id] in table 'GenreGame'
ALTER TABLE [dbo].[GenreGame]
ADD CONSTRAINT [FK_GenreGame_Genre]
    FOREIGN KEY ([Genres_Id])
    REFERENCES [dbo].[Genres]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Games_Id] in table 'GenreGame'
ALTER TABLE [dbo].[GenreGame]
ADD CONSTRAINT [FK_GenreGame_Game]
    FOREIGN KEY ([Games_Id])
    REFERENCES [dbo].[Games]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenreGame_Game'
CREATE INDEX [IX_FK_GenreGame_Game]
ON [dbo].[GenreGame]
    ([Games_Id]);
GO

-- Creating foreign key on [Game_Id] in table 'GameAchievements'
ALTER TABLE [dbo].[GameAchievements]
ADD CONSTRAINT [FK_GameGameAchievment]
    FOREIGN KEY ([Game_Id])
    REFERENCES [dbo].[Games]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GameGameAchievment'
CREATE INDEX [IX_FK_GameGameAchievment]
ON [dbo].[GameAchievements]
    ([Game_Id]);
GO

-- Creating foreign key on [Game_Id] in table 'Levels'
ALTER TABLE [dbo].[Levels]
ADD CONSTRAINT [FK_GameLevel]
    FOREIGN KEY ([Game_Id])
    REFERENCES [dbo].[Games]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GameLevel'
CREATE INDEX [IX_FK_GameLevel]
ON [dbo].[Levels]
    ([Game_Id]);
GO

-- Creating foreign key on [Survey_Id] in table 'SurveyQuestions'
ALTER TABLE [dbo].[SurveyQuestions]
ADD CONSTRAINT [FK_SurveySurveyQuestion]
    FOREIGN KEY ([Survey_Id])
    REFERENCES [dbo].[Surveys]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_SurveySurveyQuestion'
CREATE INDEX [IX_FK_SurveySurveyQuestion]
ON [dbo].[SurveyQuestions]
    ([Survey_Id]);
GO

-- Creating foreign key on [Game_Id] in table 'Surveys'
ALTER TABLE [dbo].[Surveys]
ADD CONSTRAINT [FK_GameSurvey]
    FOREIGN KEY ([Game_Id])
    REFERENCES [dbo].[Games]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GameSurvey'
CREATE INDEX [IX_FK_GameSurvey]
ON [dbo].[Surveys]
    ([Game_Id]);
GO

-- Creating foreign key on [Question_Id] in table 'SurveyQuestionAnswers'
ALTER TABLE [dbo].[SurveyQuestionAnswers]
ADD CONSTRAINT [FK_SurveyQuestionSurveyQuestionAnswer]
    FOREIGN KEY ([Question_Id])
    REFERENCES [dbo].[SurveyQuestions]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_SurveyQuestionSurveyQuestionAnswer'
CREATE INDEX [IX_FK_SurveyQuestionSurveyQuestionAnswer]
ON [dbo].[SurveyQuestionAnswers]
    ([Question_Id]);
GO

-- Creating foreign key on [Games_Id] in table 'GameFacility'
ALTER TABLE [dbo].[GameFacility]
ADD CONSTRAINT [FK_GameFacility_Game]
    FOREIGN KEY ([Games_Id])
    REFERENCES [dbo].[Games]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Facilities_Id] in table 'GameFacility'
ALTER TABLE [dbo].[GameFacility]
ADD CONSTRAINT [FK_GameFacility_Facility]
    FOREIGN KEY ([Facilities_Id])
    REFERENCES [dbo].[Facilities]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GameFacility_Facility'
CREATE INDEX [IX_FK_GameFacility_Facility]
ON [dbo].[GameFacility]
    ([Facilities_Id]);
GO

-- Creating foreign key on [GameAchievements_Id] in table 'GameAchievementUser'
ALTER TABLE [dbo].[GameAchievementUser]
ADD CONSTRAINT [FK_GameAchievementUser_GameAchievement]
    FOREIGN KEY ([GameAchievements_Id])
    REFERENCES [dbo].[GameAchievements]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Users_Id] in table 'GameAchievementUser'
ALTER TABLE [dbo].[GameAchievementUser]
ADD CONSTRAINT [FK_GameAchievementUser_User]
    FOREIGN KEY ([Users_Id])
    REFERENCES [dbo].[Users]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GameAchievementUser_User'
CREATE INDEX [IX_FK_GameAchievementUser_User]
ON [dbo].[GameAchievementUser]
    ([Users_Id]);
GO

-- Creating foreign key on [Id] in table 'GameAchievements_NumericAchievement'
ALTER TABLE [dbo].[GameAchievements_NumericAchievement]
ADD CONSTRAINT [FK_NumericAchievement_inherits_GameAchievement]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[GameAchievements]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Id] in table 'GameAchievements_TextAchievement'
ALTER TABLE [dbo].[GameAchievements_TextAchievement]
ADD CONSTRAINT [FK_TextAchievement_inherits_GameAchievement]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[GameAchievements]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Id] in table 'GameAchievements_LogicAchievement'
ALTER TABLE [dbo].[GameAchievements_LogicAchievement]
ADD CONSTRAINT [FK_LogicAchievement_inherits_GameAchievement]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[GameAchievements]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Id] in table 'GameAchievements_TimeAchievement'
ALTER TABLE [dbo].[GameAchievements_TimeAchievement]
ADD CONSTRAINT [FK_TimeAchievement_inherits_GameAchievement]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[GameAchievements]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Id] in table 'SurveyQuestionAnswers_YesNoSurveyQuestionAnswer'
ALTER TABLE [dbo].[SurveyQuestionAnswers_YesNoSurveyQuestionAnswer]
ADD CONSTRAINT [FK_YesNoSurveyQuestionAnswer_inherits_SurveyQuestionAnswer]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[SurveyQuestionAnswers]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Id] in table 'SurveyQuestionAnswers_TextSurveyQuestionAnswer'
ALTER TABLE [dbo].[SurveyQuestionAnswers_TextSurveyQuestionAnswer]
ADD CONSTRAINT [FK_TextSurveyQuestionAnswer_inherits_SurveyQuestionAnswer]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[SurveyQuestionAnswers]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------