﻿using System;
using System.Data.Entity;

namespace GameStatsTracker.DataAccess
{
	public class EntityAccessor : IDisposable
	{
		private readonly DatabaseSchemaContainer _context = new DatabaseSchemaContainer();

		public IDbSet<User> Users => _context.Users;

		public IDbSet<Game> Games => _context.Games;

		public IDbSet<Facility> Facilities => _context.Facilities;

		public IDbSet<GameAchievement> Achievements => _context.GameAchievements;

		public IDbSet<Genre> Genres => _context.Genres;


		public void Save()
		{
			_context.SaveChanges();
		}

		public void Dispose()
		{
			_context?.Dispose();
		}
	}
}