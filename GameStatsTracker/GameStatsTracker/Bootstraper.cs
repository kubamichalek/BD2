﻿using Autofac;
using GameStatsTracker.DataAccess;
using GameStatsTracker.Logic;
using GameStatsTracker.ViewModels;
using GameStatsTracker.Views;
using GameStatsTracker.Views.Core;
using IContainer = Autofac.IContainer;

namespace GameStatsTracker
{
	public class Bootstraper
	{
		private readonly ContainerBuilder _containerBuilder;

		public IContainer Container { get; }

		public Bootstraper()
		{
			_containerBuilder = new ContainerBuilder();

			RegisterDatabase();
			RegisterLogicModule();
			RegisterViewModels();
			RegisterViews();
			RegisterTypes();

			Container = _containerBuilder.Build();
		}

		private void RegisterLogicModule()
		{
			_containerBuilder.RegisterModule<LogicModule>();
		}

		private void RegisterDatabase()
		{
			_containerBuilder.RegisterModule<DbModule>();
		}

		private void RegisterViewModels()
		{
			_containerBuilder.RegisterModule<ViewModelsModule>();
		}

		private void RegisterViews()
		{
			_containerBuilder.RegisterType<ViewHost>().As<IViewHost>().SingleInstance();
			_containerBuilder.RegisterModule<ViewsModule>();
		}

		private void RegisterTypes()
		{
			_containerBuilder.RegisterType<ViewModelLocator>();
		}
	}
}