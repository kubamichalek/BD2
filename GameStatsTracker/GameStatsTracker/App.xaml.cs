﻿using Autofac;
using GameStatsTracker.ViewModels;
using GameStatsTracker.Views;
using System.Windows;

namespace GameStatsTracker
{
	/// <summary>
	/// Interaction logic for App.xaml
	/// </summary>
	public partial class App
	{
		private IContainer _container;

		protected override void OnStartup(StartupEventArgs e)
		{
			base.OnStartup(e);
			var bootstrap = new Bootstraper();
			_container = bootstrap.Container;
			Resources["Locator"] = _container.Resolve<ViewModelLocator>();

			var mainWindow = _container.Resolve<MainWindow>();
			mainWindow.Show();
		}

		protected override void OnExit(ExitEventArgs e)
		{
			base.OnExit(e);
			_container?.Dispose();
		}
	}
}
