﻿using Autofac.Features.Indexed;
using GameStatsTracker.ViewModels.PagesViewModels;
using System.Dynamic;

namespace GameStatsTracker.ViewModels
{
	public class ViewModelLocator : DynamicObject
	{
		private readonly IIndex<string, BasePageViewModel> _index;

		public ViewModelLocator(IIndex<string, BasePageViewModel> index)
		{
			_index = index;
		}

		public override bool TryGetMember(GetMemberBinder binder, out object result)
		{
			BasePageViewModel foundViewModel;
			bool isFound = _index.TryGetValue(binder.Name, out foundViewModel);
			result = foundViewModel;
			return isFound;
		}
	}
}