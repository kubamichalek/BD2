﻿using GameStatsTracker.ViewModels.PagesViewModels;
using GameStatsTracker.Views.Core;
using System.Windows.Controls;

namespace GameStatsTracker.ViewModels
{
	public class MainViewModel : BasePageViewModel
	{
		private readonly IViewHost _viewHost;

		public MainViewModel(IViewHost viewHost)
		{
			_viewHost = viewHost;
			_viewHost.GoToView(ViewNames.MAIN_MENU_PAGE);
			_viewHost.PropertyChanged += (sender, args) =>
			{
				if (args.PropertyName == nameof(CurrentView))
					RaisePropertyChanged(nameof(CurrentView));
			};
		}

		public ContentControl CurrentView => _viewHost.CurrentView;
	}
}
