﻿using GameStatsTracker.ViewModel.Helper;
using GameStatsTracker.Views.Core;
using System.Windows.Input;

namespace GameStatsTracker.ViewModels.PagesViewModels
{
	public class AboutPageViewModel : BasePageViewModel
	{
		private readonly IViewHost _viewHost;

		public AboutPageViewModel(IViewHost viewHost)
		{
			_viewHost = viewHost;
			InitializeCommands();
		}

		public ICommand BackToMenuCommand { get; set; }

		private void InitializeCommands()
		{
			BackToMenuCommand = new BaseCommand(o => GoToMainMenu());
		}

		private void GoToMainMenu()
		{
			_viewHost.GoToView(ViewNames.MAIN_MENU_PAGE);
		}
	}
}
