﻿using GameStatsTracker.DataAccess;
using GameStatsTracker.Logic;
using GameStatsTracker.ViewModel.Helper;
using GameStatsTracker.Views.Core;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Input;

namespace GameStatsTracker.ViewModels.PagesViewModels
{
	public class SignUpPageViewModel : BasePageViewModel
	{
		private readonly GameStatsProvider _dataProvider;
		private readonly IViewHost _viewHost;
		private bool _emailIsInvalid;

		public SignUpPageViewModel(GameStatsProvider dataProvider, IViewHost viewHost)
		{
			_dataProvider = dataProvider;
			_viewHost = viewHost;
			InitializeCommands();
		}

		public string Email { get; set; }
		public string Nickname { get; set; }
		public string Password { get; set; }
		public string RepeatedPassword { get; set; }
		public DateTime? Birthday { get; set; }

		public ICommand SignUpCommand { get; set; }
		public ICommand BackToMenuCommand { get; set; }

		private void InitializeCommands()
		{
			SignUpCommand = new BaseCommand(o => SignUp());
			BackToMenuCommand = new BaseCommand(o => GoToMainMenu());
		}

		private void GoToMainMenu()
		{
			_viewHost.GoToView("MainMenu");
		}

		private void SignUp()
		{
			bool isPasswordTheSame = PasswordAreTheSame();

			if (!EveryFieldIsFilled())
			{
				MessageBox.Show("Not every field was filled, check it :)");
				return;
			}

			if (!IsValidEmail(Email))
			{
				MessageBox.Show("Your email is invalid.");
				return;
			}

			if (UserAlreadyExists())
			{
				MessageBox.Show("Nickname or email you have provided is already in use.");
				return;
			}

			if (!isPasswordTheSame)
			{
				MessageBox.Show("Passwords are not the same, fix it!");
				return;
			}

			if (Password.Length < 6)
			{
				MessageBox.Show("Password is too short, it must be at least 6 characters long.");
				return;
			}

			if (Birthday.Value.Date >= DateTime.Today.Subtract(TimeSpan.FromDays(365 * 5)))
			{
				MessageBox.Show("I think you've chosen a bad date.");
				return;
			}

			_dataProvider.RegisterUser().SignUp(new User
			{
				Email = Email,
				DateOfBirth = Birthday.Value,
				NickName = Nickname,
				Password = Password,
				Role = UserRole.Player
			});

			MessageBox.Show($"Hi {Nickname}, you've been registered to GameStatsTracker!");
			_viewHost.GoToView(ViewNames.MAIN_MENU_PAGE);
		}

		private bool UserAlreadyExists()
		{
			IEnumerable<User> users = _dataProvider.GetUsers().All();
			return (users.Any(user => user.Email == Email) || users.Any(user => user.NickName == Nickname));
		}

		private bool EveryFieldIsFilled()
		{
			return !(string.IsNullOrEmpty(Email) || string.IsNullOrEmpty(Nickname) || string.IsNullOrEmpty(Password) || Birthday == null);
		}

		private bool PasswordAreTheSame()
		{
			return Password?.Equals(RepeatedPassword) ?? false;
		}

		public bool IsValidEmail(string strIn)
		{
			_emailIsInvalid = false;
			if (String.IsNullOrEmpty(strIn))
				return false;

			try
			{
				strIn = Regex.Replace(strIn, @"(@)(.+)$", DomainMapper,
					RegexOptions.None, TimeSpan.FromMilliseconds(200));
			}
			catch (RegexMatchTimeoutException)
			{
				return false;
			}

			if (_emailIsInvalid)
				return false;

			try
			{
				return Regex.IsMatch(strIn,
					@"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
					@"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
					RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
			}
			catch (RegexMatchTimeoutException)
			{
				return false;
			}
		}

		private string DomainMapper(Match match)
		{
			var idn = new IdnMapping();

			string domainName = match.Groups[2].Value;
			try
			{
				domainName = idn.GetAscii(domainName);
			}
			catch (ArgumentException)
			{
				_emailIsInvalid = true;
			}
			return match.Groups[1].Value + domainName;
		}
	}
}
