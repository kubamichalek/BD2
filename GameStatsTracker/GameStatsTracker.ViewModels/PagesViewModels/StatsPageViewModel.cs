﻿using GameStatsTracker.ViewModel.Helper;
using GameStatsTracker.Views.Core;
using System.Windows.Input;
using GameStatsTracker.Logic.Session;
using GameStatsTracker.Logic;
using System.Windows.Controls;
using System.Windows;

namespace GameStatsTracker.ViewModels.PagesViewModels
{
	public class StatsPageViewModel : BasePageViewModel
	{
		private readonly IViewHost _viewHost;
        private readonly ISessionManager _sessionManager;
        private readonly GameStatsProvider _dataProvider;

		public StatsPageViewModel(IViewHost viewHost, ISessionManager sessionManager, GameStatsProvider dataProvider)
		{
			_viewHost = viewHost;
            _sessionManager = sessionManager;
            _dataProvider = dataProvider;
			InitializeCommands();
		}

        public string GameNameOrUserName { get; set; }
        public string ElementName { get; set; }
        public string Path { get; set; }
        public ComboBoxItem SelectedTarget { get; set; }

        public ICommand BackToMenuCommand { get; set; }
        public ICommand SeeStatsCommand { get; set; }

        private void InitializeCommands()
		{
			BackToMenuCommand = new BaseCommand(o => GoToMainMenu());
            SeeStatsCommand = new BaseCommand(o => GoToStats());

        }

		private void GoToMainMenu()
		{
            if (_sessionManager.LoggedUser != null)
			    _viewHost.GoToView(ViewNames.LOGGED_USER_MENU_PAGE);
            else
                _viewHost.GoToView(ViewNames.MAIN_MENU_PAGE);
        }

        private void GoToStats()
        {
            switch (SelectedTarget.Content)
            {
                case "Game":
                    _sessionManager.MyGame = _dataProvider.GetGames().WithName(GameNameOrUserName);
                    _viewHost.GoToView(ViewNames.GAME_STATS_PAGE);
                    break;
                case "Player":
                    _sessionManager.LoggedUser = _dataProvider.GetUser().WithEmailOrNickName(GameNameOrUserName);
                    _viewHost.GoToView(ViewNames.PLAYER_STATS_PAGE);
                    break;
            }

        }
	}
}
