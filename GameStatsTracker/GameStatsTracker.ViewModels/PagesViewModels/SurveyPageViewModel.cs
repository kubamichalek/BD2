﻿using GameStatsTracker.Logic;
using GameStatsTracker.ViewModel.Helper;
using GameStatsTracker.Views.Core;
using System.Windows.Input;

namespace GameStatsTracker.ViewModels.PagesViewModels
{
	class SurveyPageViewModel : BasePageViewModel
	{
		private readonly IViewHost _viewHost;
		private readonly GameStatsProvider _dataProvider;

		private string _PageTitle;
		public string PageTitle
		{
			get { return _PageTitle; }
			set { _PageTitle = value; }
		}

		public SurveyPageViewModel(IViewHost viewHost, GameStatsProvider dataProvider)
		{
			_viewHost = viewHost;
			_dataProvider = dataProvider;
			PageTitle = "<SurveyName>";
			InitializeCommands();
		}

		public ICommand SubmitSurveyCommand { get; set; }
		public ICommand ReturnToSurveysListCommand { get; set; }

		private void InitializeCommands()
		{
			SubmitSurveyCommand = new BaseCommand(o => SubmitSurvey());
			ReturnToSurveysListCommand = new BaseCommand(o => ReturnToSurveysList());
		}

		private void SubmitSurvey()
		{
			// TODO: check validation and save survey in DB
			_viewHost.GoToView(ViewNames.SURVEY_RESULT_PAGE);
		}

		private void ReturnToSurveysList()
		{
			_viewHost.GoToView(ViewNames.SURVEYS_LIST_PAGE);
		}
	}
}
