﻿using GameStatsTracker.ViewModel.Helper;
using GameStatsTracker.Views.Core;
using System.Windows.Input;
using GameStatsTracker.Logic.Session;

namespace GameStatsTracker.ViewModels.PagesViewModels
{
	public class MainMenuViewModel : BasePageViewModel
	{
		private readonly IViewHost _viewHost;
		public ICommand NavigateToStatsPageCommand { get; set; }
		public ICommand NavigateToSignInPageCommand { get; set; }
		public ICommand NavigateToSignUpPageCommand { get; set; }
		public ICommand NavigateToAboutPageCommand { get; set; }
        private ISessionManager _sessionManager;

		public MainMenuViewModel(IViewHost viewHost, ISessionManager sessionManager)
		{
			_viewHost = viewHost;
            _sessionManager = sessionManager;
            _sessionManager.LoggedOut = true;
			InitializeCommands();
		}

		private void InitializeCommands()
		{
			NavigateToStatsPageCommand = new BaseCommand(o => _viewHost.GoToView(ViewNames.STATS_PAGE));
			NavigateToSignInPageCommand = new BaseCommand(o => _viewHost.GoToView(ViewNames.SIGN_IN_PAGE));
			NavigateToSignUpPageCommand = new BaseCommand(o => _viewHost.GoToView(ViewNames.SIGN_UP_PAGE));
			NavigateToAboutPageCommand = new BaseCommand(o => _viewHost.GoToView(ViewNames.ABOUT_PAGE));
		}
	}
}
