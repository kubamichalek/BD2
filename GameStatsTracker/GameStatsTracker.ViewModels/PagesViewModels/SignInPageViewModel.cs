﻿using GameStatsTracker.Logic;
using GameStatsTracker.ViewModel.Helper;
using GameStatsTracker.Logic.Session;
using GameStatsTracker.Views.Core;
using System.Windows;
using System.Windows.Input;

namespace GameStatsTracker.ViewModels.PagesViewModels
{
	public class SignInPageViewModel : BasePageViewModel
	{
		private readonly GameStatsProvider _dataProvider;
		private readonly IViewHost _viewHost;
        private readonly ISessionManager _sessionManager;

		public SignInPageViewModel(IViewHost viewHost, GameStatsProvider dataProvider, ISessionManager sessionManager)
		{
			_viewHost = viewHost;
			_dataProvider = dataProvider;
            _sessionManager = sessionManager;
			InitializeCommands();
		}

		public string EmailOrNickname { get; set; }
		public string Password { get; set; }
		public ICommand SignInCommand { get; set; }
		public ICommand BackToMenuCommand { get; set; }

		private void InitializeCommands()
		{
			SignInCommand = new BaseCommand(o => SignIn());
			BackToMenuCommand = new BaseCommand(o => GoToMainMenu());
		}

		private void GoToMainMenu()
		{
			_viewHost.GoToView(ViewNames.MAIN_MENU_PAGE);
		}

		private void SignIn()
		{
			if (string.IsNullOrEmpty(EmailOrNickname) || string.IsNullOrEmpty(Password))
			{
				MessageBox.Show("Fill all the fields, please");
				return;
			}

			bool userExists = _dataProvider
				.GetUsers()
				.HavingEmailOrNicknameAndPassword(EmailOrNickname, Password);

			if (!userExists)
			{
				MessageBox.Show($"{EmailOrNickname} you're not registered, please sign up first");
				_viewHost.GoToView(ViewNames.SIGN_UP_PAGE);
				return;
			}
            _sessionManager.LoggedUser = _dataProvider.GetUser().WithEmailOrNickName(EmailOrNickname);
            _sessionManager.LoggedOut = false;
			_viewHost.GoToView(ViewNames.LOGGED_USER_MENU_PAGE);
		}
	}
}
