﻿using GameStatsTracker.DataAccess;
using GameStatsTracker.Logic;
using GameStatsTracker.Logic.Session;
using GameStatsTracker.ViewModel.Helper;
using GameStatsTracker.Views.Core;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Input;

namespace GameStatsTracker.ViewModels.PagesViewModels
{
    public class PlayerStatsPageViewModel : BasePageViewModel
    {
        private readonly IViewHost _viewHost;
        private readonly GameStatsProvider _dataProvider;
        private readonly ISessionManager _sessionManager;

        private string _PageTitle;
        public string PageTitle
        {
            get { return _PageTitle; }
            set { Set(ref _PageTitle, value); }
        }
        public string PlayerAvatarSource { get; set; }

        private Game _selectedGame;

        public ICollection<Game> UserGames { get; }
        public Game SelectedGame
        {
            get => _selectedGame;
            set
            {
                Set(ref _selectedGame, value);
                RaisePropertyChanged(string.Empty);
            }
        }
        public ICollection<Facility> GameFacilities => SelectedGame?.Facilities;

        public IEnumerable<GameAchievement> GameAchievements => _sessionManager.LoggedUser.GameAchievements.Where(a=>a.Game.Id == SelectedGame?.Id);

        private GameAchievement _selectedAchievements;
        public GameAchievement SelectedAchievements
        {
            get => _selectedAchievements;
            set
            {
                Set(ref _selectedAchievements, value);
                _sessionManager.UserGameAchievement = _selectedAchievements;
                _viewHost.GoToView(ViewNames.ACHIEVEMENT_PAGE);
            }
        }

        public PlayerStatsPageViewModel(IViewHost viewHost, GameStatsProvider dataProvider, ISessionManager sessionManager)
        {
            _viewHost = viewHost;
            _dataProvider = dataProvider;
            _sessionManager = sessionManager;
            PageTitle = $"{_sessionManager.LoggedUser.NickName}";
            PlayerAvatarSource = "../Resources/playersAvatar.jpg";
            UserGames = _sessionManager.LoggedUser.Games;
            InitializeCommands();
        }

        public ICommand BackToMenuCommand { get; set; }

        private void InitializeCommands()
        {
            BackToMenuCommand = new BaseCommand(o => GoToMainMenu());
        }

        private void GoToMainMenu()
        {
            if (_sessionManager.LoggedOut == false)
                _viewHost.GoToView(ViewNames.LOGGED_USER_MENU_PAGE);
            else
            {
                _sessionManager.LoggedOut = true;
                _viewHost.GoToView(ViewNames.MAIN_MENU_PAGE);
            }
        }
    }
}
