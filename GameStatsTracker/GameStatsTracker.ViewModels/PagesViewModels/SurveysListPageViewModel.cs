﻿using GameStatsTracker.Logic;
using GameStatsTracker.ViewModel.Helper;
using GameStatsTracker.Views.Core;
using System.Windows.Input;


namespace GameStatsTracker.ViewModels.PagesViewModels
{
	class SurveysListPageViewModel : BasePageViewModel
	{
		private readonly IViewHost _viewHost;
		private readonly GameStatsProvider _dataProvider;

		private string _PageTitle;
		public string PageTitle
		{
			get { return _PageTitle; }
			set { _PageTitle = value; }
		}

		public SurveysListPageViewModel(IViewHost viewHost, GameStatsProvider dataProvider)
		{
			_viewHost = viewHost;
			_dataProvider = dataProvider;
			PageTitle = "Recent surveys";
			InitializeCommands();
		}

		public ICommand GoToSurveyCommand { get; set; }
		public ICommand BackToMenuCommand { get; set; }

		private void InitializeCommands()
		{
			GoToSurveyCommand = new BaseCommand(o => GoToSurvey());
			BackToMenuCommand = new BaseCommand(o => GoToMainMenu());
		}

		private void GoToSurvey()
		{
			_viewHost.GoToView(ViewNames.SURVEY_PAGE);
		}

		private void GoToMainMenu()
		{
			_viewHost.GoToView(ViewNames.LOGGED_USER_MENU_PAGE);
		}
	}
}
