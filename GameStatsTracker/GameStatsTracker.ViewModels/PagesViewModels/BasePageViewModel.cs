﻿using GameStatsTracker.ViewModels.Helpers;
using System.Runtime.CompilerServices;

namespace GameStatsTracker.ViewModels.PagesViewModels
{
	public class BasePageViewModel : ObservableObject
	{
        protected void Set<T>(ref T _field, T value, [CallerMemberName] string caller = "")
        {
            _field = value;
            RaisePropertyChanged(caller);
        }
	}
}
