﻿using GameStatsTracker.Logic;
using GameStatsTracker.Logic.Session;
using GameStatsTracker.ViewModel.Helper;
using GameStatsTracker.Views.Core;
using System.Windows;
using System.Windows.Input;

namespace GameStatsTracker.ViewModels.PagesViewModels
{
	public class LoggedUserMenuPageViewModel : BasePageViewModel
	{
		private readonly IViewHost _viewHost;
		private readonly GameStatsProvider _dataProvider;
        private readonly ISessionManager _sessionManager;

		private string _pageTitle;

		public string PageTitle
		{
			get { return _pageTitle; }
			set { Set(ref _pageTitle, value); }
		}

		public LoggedUserMenuPageViewModel(IViewHost viewHost, GameStatsProvider dataProvider, ISessionManager sessionManager)
		{
			_viewHost = viewHost;
			_dataProvider = dataProvider;
            _sessionManager = sessionManager;
			PageTitle = $"Welcome, {_sessionManager.LoggedUser.NickName}";
			InitializeCommands();
		}

		public ICommand ShowMyStatsCommand { get; set; }
		public ICommand ShowStatsCommand { get; set; }
		public ICommand ShowSurveysCommand { get; set; }
		public ICommand SignOutCommand { get; set; }

		private void InitializeCommands()
		{
			ShowMyStatsCommand = new BaseCommand(o => ShowMyStats());
			ShowStatsCommand = new BaseCommand(o => ShowStats());
			ShowSurveysCommand = new BaseCommand(o => ShowSurveys());
			SignOutCommand = new BaseCommand(o => SignOut());
		}

		private void ShowMyStats()
		{
			// TODO: set redirection to current user's stats window
			_viewHost.GoToView(ViewNames.PLAYER_STATS_PAGE);
		}

		private void ShowStats()
		{
			_viewHost.GoToView(ViewNames.STATS_PAGE);
		}

		private void ShowSurveys()
		{
			_viewHost.GoToView(ViewNames.SURVEYS_LIST_PAGE);
		}

		private void SignOut()
		{
            _sessionManager.LoggedUser = null;
            _sessionManager.LoggedOut = true;
			MessageBox.Show("You've successfully signed out.");
			_viewHost.GoToView(ViewNames.MAIN_MENU_PAGE);
		}
	}
}