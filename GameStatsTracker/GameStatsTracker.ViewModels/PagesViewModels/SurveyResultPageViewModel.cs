﻿using GameStatsTracker.Logic;
using GameStatsTracker.ViewModel.Helper;
using GameStatsTracker.Views.Core;
using System.Windows.Input;

namespace GameStatsTracker.ViewModels.PagesViewModels
{
	class SurveyResultPageViewModel : BasePageViewModel
	{
		private readonly IViewHost _viewHost;
		private readonly GameStatsProvider _dataProvider;

		private string _PageTitle;
		public string PageTitle
		{
			get { return _PageTitle; }
			set { _PageTitle = value; }
		}

		public SurveyResultPageViewModel(IViewHost viewHost, GameStatsProvider dataProvider)
		{
			_viewHost = viewHost;
			_dataProvider = dataProvider;
			PageTitle = "<SurveyName>\nResults";
			InitializeCommands();
		}

		public ICommand ReturnToSurveysListCommand { get; set; }

		private void InitializeCommands()
		{
			ReturnToSurveysListCommand = new BaseCommand(o => ReturnToSurveysList());
		}


		private void ReturnToSurveysList()
		{
			_viewHost.GoToView(ViewNames.SURVEYS_LIST_PAGE);
		}
	}
}
