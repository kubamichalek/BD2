﻿using GameStatsTracker.Logic;
using GameStatsTracker.ViewModel.Helper;
using GameStatsTracker.Views.Core;
using System.Windows.Input;
using GameStatsTracker.Logic.Session;
using GameStatsTracker.DataAccess;

namespace GameStatsTracker.ViewModels.PagesViewModels
{
	class AchievementPageViewModel : BasePageViewModel
	{
		private readonly IViewHost _viewHost;
		private readonly GameStatsProvider _dataProvider;
        private readonly ISessionManager _sessionManager;

		private string _PageTitle;
		public string PageTitle
		{
			get { return _PageTitle; }
			set { _PageTitle = value; }
		}

		public GameAchievement AchievementDetails { get; }

		public AchievementPageViewModel(IViewHost viewHost, GameStatsProvider dataProvider, ISessionManager sessionManager)
		{
			_viewHost = viewHost;
			_dataProvider = dataProvider;
            _sessionManager = sessionManager;
			PageTitle = $"{_sessionManager.UserGameAchievement.Name}";
			AchievementDetails = _sessionManager.UserGameAchievement;
			InitializeCommands();
		}

		public ICommand BackToMenuCommand { get; set; }

		private void InitializeCommands()
		{
			BackToMenuCommand = new BaseCommand(o => GoToGameStatsPage());
		}

		private void GoToGameStatsPage()
		{
            if (_sessionManager.LoggedOut == false)
                _viewHost.GoToView(ViewNames.PLAYER_STATS_PAGE);
            else
                _viewHost.GoToView(ViewNames.GAME_STATS_PAGE);
		}
	}
}
