﻿using GameStatsTracker.Logic;
using GameStatsTracker.ViewModel.Helper;
using GameStatsTracker.Views.Core;
using System.Windows.Input;
using GameStatsTracker.Logic.Session;
using System.Collections.Generic;
using GameStatsTracker.DataAccess;

namespace GameStatsTracker.ViewModels.PagesViewModels
{
	public class GameStatsPageViewModel : BasePageViewModel
	{
		private readonly IViewHost _viewHost;
		private readonly GameStatsProvider _dataProvider;
        private readonly ISessionManager _sessionManager;

		private string _PageTitle;
		public string PageTitle
		{
			get { return _PageTitle; }
            set { Set(ref _PageTitle, value); }
        }

		private string _GameCoverSource;
		public string GameCoverSource
		{
			get { return _GameCoverSource; }
			set { _GameCoverSource = value; }
		}

		private string _GameDetails;
		public string GameDetails
		{
			get { return _GameDetails; }
			set { _GameDetails = value; }
		}

		private string _GameDescription;
		public string GameDescription
		{
			get { return _GameDescription; }
			set { _GameDescription = value; }
		}

        public ICollection<GameAchievement> Achievements { get; }
        public ICollection<Facility> Facilities { get; }
        public ICollection<User> Players { get; }
        public ICollection<Level> Levels { get; }
        public ICollection<Survey> Surveys { get; }
        private GameAchievement _selectedAchievements;
        public GameAchievement SelectedAchievements
        {
            get => _selectedAchievements;
            set
            {
                Set(ref _selectedAchievements, value);
                _sessionManager.UserGameAchievement = _selectedAchievements;
                _viewHost.GoToView(ViewNames.ACHIEVEMENT_PAGE);
            }
        }

        public GameStatsPageViewModel(IViewHost viewHost, GameStatsProvider dataProvider, ISessionManager sessionManager)
		{
			_viewHost = viewHost;
			_dataProvider = dataProvider;
            _sessionManager = sessionManager;
			PageTitle = $"{_sessionManager.MyGame.Name}";
			GameCoverSource = "../Resources/gameCover.jpg";
			GameDetails = $"Relase date : {_sessionManager.MyGame.ReleaseDate}";
            GameDescription = $"{_sessionManager.MyGame.Description}";
            Achievements = _sessionManager.MyGame.Achievements;
            Facilities = _sessionManager.MyGame.Facilities;
            Players = _sessionManager.MyGame.Users;
            Levels = _sessionManager.MyGame.Levels;
            Surveys = _sessionManager.MyGame.Surveys;

            InitializeCommands();
		}

		public ICommand BackToMenuCommand { get; set; }

		private void InitializeCommands()
		{
			BackToMenuCommand = new BaseCommand(o => GoToMainMenu());
		}

		private void GoToMainMenu()
		{
            if (_sessionManager.LoggedOut == true)
                _viewHost.GoToView(ViewNames.MAIN_MENU_PAGE);
            else
                _viewHost.GoToView(ViewNames.LOGGED_USER_MENU_PAGE);
		}
	}
}
