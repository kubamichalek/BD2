﻿using Autofac;
using GameStatsTracker.ViewModels.PagesViewModels;
using System.Reflection;
using Module = Autofac.Module;

namespace GameStatsTracker.ViewModels
{
	public class ViewModelsModule : Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly())
				.Where(type => type.IsAssignableTo<BasePageViewModel>())
				.Keyed<BasePageViewModel>(type => type.Name.Replace("ViewModel", string.Empty))
				.InstancePerDependency();
		}
	}
}