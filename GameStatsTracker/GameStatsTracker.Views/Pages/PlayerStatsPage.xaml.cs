﻿using GameStatsTracker.ViewModels.PagesViewModels;
using GameStatsTracker.Views.Core;
using System.Windows;
using System.Windows.Controls;

namespace GameStatsTracker.Views.Pages
{
    /// <summary>
    /// Interaction logic for PlayerStatsPage.xaml
    /// </summary>
    [PageName(ViewNames.PLAYER_STATS_PAGE)]
    public partial class PlayerStatsPage : UserControl
    {
        public PlayerStatsPage()
        {
            InitializeComponent();
        }
    }
}
