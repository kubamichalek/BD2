﻿using GameStatsTracker.ViewModels.PagesViewModels;
using GameStatsTracker.Views.Core;
using System.Windows;
using System.Windows.Controls;

namespace GameStatsTracker.Views.Pages
{
	/// <summary>
	/// Interaction logic for SignUpPage.xaml
	/// </summary>
	[PageName(ViewNames.SIGN_UP_PAGE)]
	public partial class SignUpPage
	{
		public SignUpPage()
		{
			InitializeComponent();
		}

		private void PasswordBox_OnPasswordChanged(object sender, RoutedEventArgs e)
		{
			var viewModel = DataContext as SignUpPageViewModel;
			var passwordBox = sender as PasswordBox;
			if (viewModel == null)
				return;
			viewModel.Password = passwordBox?.Password;
		}

		private void ConfirmPasswordBox_OnPasswordChanged(object sender, RoutedEventArgs e)
		{
			var viewModel = DataContext as SignUpPageViewModel;
			var passwordBox = sender as PasswordBox;
			if (viewModel == null)
				return;
			viewModel.RepeatedPassword = passwordBox?.Password;
		}
    }
}
