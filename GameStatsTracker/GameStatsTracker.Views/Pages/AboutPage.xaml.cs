﻿using GameStatsTracker.Views.Core;

namespace GameStatsTracker.Views.Pages
{
	/// <summary>
	/// Interaction logic for AboutPage.xaml
	/// </summary>
	[PageName(ViewNames.ABOUT_PAGE)]
	public partial class AboutPage
	{
		public AboutPage()
		{
			InitializeComponent();
		}
	}
}
