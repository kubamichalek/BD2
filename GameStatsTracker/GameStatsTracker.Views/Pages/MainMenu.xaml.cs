﻿using GameStatsTracker.Views.Core;

namespace GameStatsTracker.Views.Pages
{
	/// <summary>
	/// Interaction logic for MainMenu.xaml
	/// </summary>
	[PageName(ViewNames.MAIN_MENU_PAGE)]
	public partial class MainMenu
	{
		public MainMenu()
		{
			InitializeComponent();
		}
	}
}
