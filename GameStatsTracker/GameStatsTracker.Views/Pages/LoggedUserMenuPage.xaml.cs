﻿using GameStatsTracker.Views.Core;

namespace GameStatsTracker.Views.Pages
{
	/// <summary>
	/// Logika interakcji dla klasy LoggedUserMenu.xaml
	/// </summary>
	[PageName(ViewNames.LOGGED_USER_MENU_PAGE)]
	public partial class LoggedUserMenuPage
	{
		public LoggedUserMenuPage()
		{
			InitializeComponent();
		}

    }
}
