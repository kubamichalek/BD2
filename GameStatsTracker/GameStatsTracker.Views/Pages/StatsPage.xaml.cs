﻿using GameStatsTracker.Views.Core;
using System.Windows.Controls;

namespace GameStatsTracker.Views.Pages
{
	/// <summary>
	/// Interaction logic for StatsPage.xaml
	/// </summary>
	[PageName(ViewNames.STATS_PAGE)]
	public partial class StatsPage : UserControl
	{
		public StatsPage()
		{
			InitializeComponent();
		}

    }
}
