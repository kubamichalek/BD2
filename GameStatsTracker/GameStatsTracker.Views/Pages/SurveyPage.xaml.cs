﻿using GameStatsTracker.ViewModels.PagesViewModels;
using GameStatsTracker.Views.Core;
using System.Windows;
using System.Windows.Controls;

namespace GameStatsTracker.Views.Pages
{
    /// <summary>
    /// Interaction logic for SurveyPage.xaml
    /// </summary>
    [PageName(ViewNames.SURVEY_PAGE)]
    public partial class SurveyPage : UserControl
    {
        public SurveyPage()
        {
            InitializeComponent();
        }
    }
}
