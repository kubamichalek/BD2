﻿using GameStatsTracker.ViewModels.PagesViewModels;
using GameStatsTracker.Views.Core;
using System.Windows;
using System.Windows.Controls;

namespace GameStatsTracker.Views.Pages
{
    /// <summary>
    /// Interaction logic for GameStatsPage.xaml
    /// </summary>
    [PageName(ViewNames.GAME_STATS_PAGE)]
    public partial class GameStatsPage : UserControl
    {
        public GameStatsPage()
        {
            InitializeComponent();
        }
    }
}
