﻿using GameStatsTracker.ViewModels.PagesViewModels;
using GameStatsTracker.Views.Core;
using System.Windows;
using System.Windows.Controls;

namespace GameStatsTracker.Views.Pages
{
	/// <summary>
	/// Interaction logic for SignInPage.xaml
	/// </summary>
	[PageName(ViewNames.SIGN_IN_PAGE)]
	public partial class SignInPage : UserControl
	{
		public SignInPage()
		{
			InitializeComponent();
		}

		private void PasswordBox_OnPasswordChanged(object sender, RoutedEventArgs e)
		{
			var viewModel = DataContext as SignInPageViewModel;
			var passwordBox = sender as PasswordBox;
			if (viewModel == null)
				return;
			viewModel.Password = passwordBox?.Password;
		}
	}
}
