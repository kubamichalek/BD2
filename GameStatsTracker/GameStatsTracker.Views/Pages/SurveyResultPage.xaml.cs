﻿using GameStatsTracker.ViewModels.PagesViewModels;
using GameStatsTracker.Views.Core;
using System.Windows;
using System.Windows.Controls;

namespace GameStatsTracker.Views.Pages
{
    /// <summary>
    /// Interaction logic for SurveyResultPage.xaml
    /// </summary>
    [PageName(ViewNames.SURVEY_RESULT_PAGE)]
    public partial class SurveyResultPage : UserControl
    {
        public SurveyResultPage()
        {
            InitializeComponent();
        }
    }
}
