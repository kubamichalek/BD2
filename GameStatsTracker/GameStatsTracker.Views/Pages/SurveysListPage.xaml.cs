﻿using GameStatsTracker.ViewModels.PagesViewModels;
using GameStatsTracker.Views.Core;
using System.Windows;
using System.Windows.Controls;

namespace GameStatsTracker.Views.Pages
{
    /// <summary>
    /// Interaction logic for SurveysListPage.xaml
    /// </summary>
    [PageName(ViewNames.SURVEYS_LIST_PAGE)]
    public partial class SurveysListPage : UserControl
    {
        public SurveysListPage()
        {
            InitializeComponent();
        }
    }
}
