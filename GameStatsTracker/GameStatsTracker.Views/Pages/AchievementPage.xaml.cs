﻿using GameStatsTracker.ViewModels.PagesViewModels;
using GameStatsTracker.Views.Core;
using System.Windows;
using System.Windows.Controls;

namespace GameStatsTracker.Views.Pages
{
    /// <summary>
    /// Interaction logic for AchievementPage.xaml
    /// </summary>
    [PageName(ViewNames.ACHIEVEMENT_PAGE)]
    public partial class AchievementPage : UserControl
    {
        public AchievementPage()
        {
            InitializeComponent();
        }
    }
}
