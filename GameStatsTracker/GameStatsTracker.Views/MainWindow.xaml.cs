﻿using System.Windows;
using GameStatsTracker.Views.Core;

namespace GameStatsTracker.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    [PageName(ViewNames.MAIN_WINDOW)]
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
    }
}
