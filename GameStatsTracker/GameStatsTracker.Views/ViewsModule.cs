﻿using Autofac;
using GameStatsTracker.Views.Core;
using System;
using System.Reflection;
using System.Windows.Controls;
using Module = Autofac.Module;

namespace GameStatsTracker.Views
{
	public class ViewsModule : Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly())
				.Where(type => type.IsAssignableTo<ContentControl>() && type.GetCustomAttribute<PageNameAttribute>() != null)
				.Keyed<ContentControl>(GetPageNameForType).AsSelf()
				.InstancePerDependency();
		}

		private static string GetPageNameForType(Type type)
		{
			var pageNameAttribute = type.GetCustomAttribute<PageNameAttribute>();
			return pageNameAttribute.Name;
		}
	}
}